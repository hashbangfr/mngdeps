#!/bin/bash
set -euo pipefail

# install_requires
pip-compile -o requirements.txt

# tests_require
python -c "from distutils.core import run_setup ; dist = run_setup('setup.py') ; print('\n'.join(dist.tests_require))" | pip-compile -o requirements-tests.txt -

# extras_require ; one file per extra
EXTRA_KEYS=$(python -c "from distutils.core import run_setup ; dist = run_setup('setup.py') ; print('\n'.join(dist.extras_require.keys()))" )
for key in $EXTRA_KEYS ; do 
    python -c "from distutils.core import run_setup ; dist = run_setup('setup.py') ; print('\n'.join(dist.extras_require['$key']))" | pip-compile -o requirements-$key.txt -
done

