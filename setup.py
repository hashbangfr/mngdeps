from setuptools import setup

setup(
    name="mngdeps",
    version="0.1.0.dev0",
    description='Script to help python dependencies management',
    packages=[],
    author='Pierre Charlet',
    scripts=['mngdeps.sh'],
    install_requires=[
        'pip-tools>=4.1'
        'pip>=19.2.3'
    ],
)
