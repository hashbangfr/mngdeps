mngdeps
=======

Script to help python dependencies management

.. code-block:: bash
  
  pip install git+https://gitlab.com/hashbangfr/mngdeps.git
  mngdeps.sh --check
  mngdeps.sh --update


Do not forget to update pip and pip-tools

.. code-block:: bash
  
  (venv) pip install -U pip pip-tools
